from google.cloud import vision
from google.cloud.vision_v1 import types

client = vision.ImageAnnotatorClient()

with open('./images/test4.jpeg', 'rb') as image_file:
    content = image_file.read()

image = types.Image(content=content)

response = client.annotate_image({
    'image': image
})

f = open('response.json', 'w+')
f.write(str(response))
f.close()

print(response.text_annotations[0].description)
